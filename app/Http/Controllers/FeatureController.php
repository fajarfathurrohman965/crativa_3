<?php

namespace App\Http\Controllers;

use App\Models\Feature;
use Illuminate\Http\Request;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $feature = Feature::all();

        return view('admin.feature.index', compact('feature'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $feature = null;

        return view('admin.feature.action', compact('feature'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'sub' => 'required',
            'deskripsi' => 'required',
        ]);

        Feature::create([
            'judul' => $request->judul,
            'sub' => $request->sub,
            'deskripsi' => $request->deskripsi,
        ]);

        return redirect()->route('feature.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $feature = Feature::findorfail($id);

        return view('admin.feature.action', compact('feature'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'judul' => 'required',
            'sub' => 'required',
            'deskripsi' => 'required',
        ]);

        Feature::findorfail($id)->update([
            'judul' => $request->judul,
            'sub' => $request->sub,
            'deskripsi' => $request->deskripsi,
        ]);

        return redirect()->route('feature.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $feature = Feature::findOrFail($id);
        $feature->delete();
        return redirect()->route('feature.index');
    }
}
