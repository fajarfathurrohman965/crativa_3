<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $image = Image::all();

        return view('admin.image.index', compact('image'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $image = null;

        return view('admin.image.action', compact('image'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $file = $request->file('gambar');
 
        $nama_file = time()."".$file->getClientOriginalName();
 
        $tujuan_upload = 'upload';
        $file->move($tujuan_upload,$nama_file);

        Image::create([
            'gambar' => $request->getSchemeAndHttpHost().'/upload/'.$nama_file
        ]);

        return redirect()->route('image.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $image = Image::findorfail($id);

        return view('admin.image.action', compact('image'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $file = $request->file('gambar');
 
        $nama_file = time()."".$file->getClientOriginalName();
 
        $tujuan_upload = 'upload';
        $file->move($tujuan_upload,$nama_file);

        Image::findorfail($id)->update([
            'gambar' => $request->getSchemeAndHttpHost().'/upload/'.$nama_file
        ]);

        return redirect()->route('image.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $image = Image::findOrFail($id);

        $imageName = basename($image->gambar);

        $path = public_path('upload/' . $imageName);
        if (File::exists($path)) {
        File::delete($path);
        }

        $image->delete();

        return redirect()->route('image.index');
    }
}
