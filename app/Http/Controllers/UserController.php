<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Button;
use App\Models\Contact;
use App\Models\Image;
use App\Models\Price;
use App\Models\Feature;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function Dashboard()
    {
        $feature = Feature::all();
        $image = Image::all();
        $price = Price::all();
        $contact = Contact::all();
        $banner = Banner::all();
        $button = Button::all();
        $feature1 = $feature->slice(3);
        $feature2 = $feature->slice(0, 3);

        return view('user.dashboard', compact('feature', 'image', 'price', 'contact', 'banner', 'button', 'feature1', 'feature2'));
    }
}
