<?php

namespace App\Http\Controllers;

use App\Models\Button;
use Illuminate\Http\Request;

class ButtonController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $button = Button::all();

        return view('admin.button.index', compact('button'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $button = null;

        return view('admin.button.action', compact('button'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        Button::create([
            'nama' => $request->nama,
        ]);

        return redirect()->route('button.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $button = Button::findorfail($id);

        return view('admin.button.action', compact('button'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        Button::findorfail($id)->update([
            'nama' => $request->nama
        ]);

        return redirect()->route('button.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $button = Button::findOrFail($id);
        $button->delete();
        return redirect()->route('button.index');
    }
}
