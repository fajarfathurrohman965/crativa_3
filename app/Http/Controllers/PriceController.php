<?php

namespace App\Http\Controllers;

use App\Models\Price;
use App\Models\Privileges;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $price = Price::all();
        $privilege = Privileges::all();

        return view('admin.price.index', compact('price', 'privilege'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $price = null;
        $privilege = null;

        return view('admin.price.action', compact('price', 'privilege'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $price = Price::create([
            'nama' => $request->nama,
            'harga' => $request->harga,
        ]);

        $privilege = $request->privilage;

        if (!is_null($privilege) && is_array($privilege)) {
         foreach ($privilege as $item) {
                Privileges::create([
                    'price_id' => $price->id,
                    'privilege' => $item
                ]);
            }
        }

        return redirect(route('price.index'));
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $price = Price::findorfail($id);

        return view('admin.price.action', compact('price'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $price = Price::findOrNew($id);
        $price->fill([
            'nama' => $request->nama,
            'harga' => $request->harga,
        ])->save();

        $price->privileges()->delete();
        $privilege = $request->privilage;

        if (!is_null($privilege) && is_array($privilege)) {
            foreach ($privilege as $item) {
                $price->privileges()->create([
                'privilege' => $item
                ]);
            }
        }
        return redirect()->route('price.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $price = Price::findOrFail($id);
        Privileges::where('price_id', $price->id) -> delete();
        $price->delete();
        return redirect()->route('price.index');
    }
}
