<?php

namespace App\Models;

use App\Models\Privileges;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Price extends Model
{
    use HasFactory;
    protected $table = 'price';
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];

    public function privileges()
    {
        return $this->hasMany(Privileges::class);
    }
}
