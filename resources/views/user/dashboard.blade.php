@extends('layout.user')
@section('content')
<!-- Navigation -->
<div class="nav-top px-lg-4 px-2">
    <div class="nav-inner h-100">
        <div class="container-fluid h-100">
            <div class="row h-100">
                <div class="col-4 h-100">
                    <div class="d-flex align-items-center h-100">
                        <a href="#" class="nav-logo text-white">
                            Crativa
                        </a>
                    </div>
                </div>
                <div class="col-4 h-100">
                    <div class="d-flex align-items-center justify-content-center h-100">
                        <div class="nav-top-menus nav-top-menus--secondary">
                            <a href="#featuresSection" class="menus-link me-3">Features</a>
                            <a href="#pricingSection" class="menus-link me-3">Pricing</a>
                            <a href="#contactSection" class="menus-link">Contact</a>
                        </div>
                    </div>
                </div>
                <div class="col-4 h-100">
                    <div class="d-flex align-items-center justify-content-end h-100">
                        <div class="nav-top-menus nav-top-menus--white">
                            <a href="https://instagram.com/fajar.fathurrr" class="menus-link me-3"><i class="icofont-instagram"></i></a>
                            <a href="#" class="menus-link me-3"><i class="icofont-twitter"></i></a>
                            <a href={{ Auth::user() ? '/admin' : '/login' }} class="menus-link me-3">
                                <i class="icofont-user">
                                    @if (Auth::user() == true)
                                    {{ Auth::user()->name }}
                                    @else
                                    Masuk
                                    @endif
                                </i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Section > Home Banner -->
<div class="section-offset-nav d-block py-lg-5 py-3 bg-images" style="background-image: url('{{ $image[0]->gambar }}');">
    <div class="section-have-cover container my-lg-5 my-4 px-lg-5">
        <div class="row align-items-center">
            <div class="col-md-7 mb-lg-5 mb-4 text-white">
                <h1 class="mb-3 text-xlg">{{ $banner[0]->judul }}</h1>
                <p class="mb-4 font-regular pe-lg-5">{{ $banner[0]->deskripsi }}</p>
                <a href="#" class="btn btn-lg btn-secondary px-4">{{ $button[0]->nama }} <i class="icofont-arrow-right ms-3"></i></a>
            </div>
        </div>
    </div>
    <div class="bg-images-cover"></div>
</div>
<!-- Section > Text -->
<div class="d-block bg-primary py-lg-5 py-3">
    <div class="container my-lg-5 my-4 px-lg-5 text-center">
        <div class="font-weight-bold text-lg d-block mb-4 text-secondary">
            {{ $banner[1]->judul }}
        </div>
        <a href="#" class="font-weight-bold text-secondary">{{ $button[4]->nama }}</a>
    </div>
</div>
<!-- Section > Feature -->
<div id="featuresSection" class="d-block bg-light py-lg-5 py-3">
    <div class="container my-lg-5 my-4 px-lg-5">
        <h2 class="mb-lg-5 mb-4">Features</h2>
        <div class="row">
            @foreach ($feature2 as $features)
            <div class="col-md-4 mb-lg-0 mb-3">
                <div class="d-flex flex-column pb-3 mb-3 border-bottom border-primary h-100">
                    <div class="text-lg font-thin text-primary d-block mb-3">{{ $features->judul }}</div>
                    <div class="flex-1">
                        <h4 class="mb-3">{{ $features->sub }}</h4>
                        <p class="small">{{ $features->deskripsi }}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Section -->
<div class="d-block bg-secondary py-lg-5 py-3">
    <div class="container my-lg-5 my-4 px-lg-5 text-center">
        <div class="d-block text-lg font-thin mb-5">
            {{ $banner[2]->judul }}
        </div>
    </div>
</div>
<!-- Section > Content -->
<div class="d-block py-lg-5 py-3">
    <div class="container my-lg-5 my-4 px-lg-5">
        <div class="row align-items-center">
            <div class="col-md-6 order-md-2 mb-lg-0 mb-4">
                <div class="block-img rounded" style="background-image: url('{{ $image[3]->gambar }}');"></div>
            </div>
            <div class="col-md-6 order-md-1 mb-lg-0 mb-4">
                <div class="d-block pe-lg-5">
                    <h2 class="mb-3">{{ $banner[3]->judul }}</h2>
                    <p class="small m-0">{{ $banner[3]->deskripsi }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Section > Content -->
<div class="d-block py-lg-5 py-3">
    <div class="container my-lg-5 my-4 px-lg-5">
        <div class="row align-items-center">
            <div class="col-md-6 mb-lg-0 mb-3">
                <div class="block-img rounded" style="background-image: url('{{ $image[4]->gambar }}');"></div>
            </div>
            <div class="col-md-6 mb-lg-0 mb-3">
                <div class="d-block ps-lg-5">
                    <h2 class="mb-3">{{ $banner[4]->judul }}</h2>
                    <p class="small m-0">{{ $banner[4]->deskripsi }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Section > Content -->
<div class="d-block bg-light py-lg-5 py-3">
    <div class="container my-lg-5 my-4 px-lg-5">
        <div class="row">
            @foreach ($feature1 as $features)
            <div class="col-md-3">
                <h6>{{ $features->judul }}</h6>
                <div class="d-flex flex-column py-3 my-3 border-top border-bottom border-primary h-100">
                    <div class="flex-1">
                        <h4>{{ $features->sub }}</h4>
                        <div><p class="small">{{ $features->deskripsi }}</p></div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
 <!-- Section > Content -->
 <div class="d-block bg-images py-lg-5 py-3 text-white border-bottom border-thick border-secondary" style="background-image: url('{{ $image[3]->gambar }}');">
    <div class="section-have-cover container my-lg-5 my-4 px-lg-5 text-center">
        <h1 class="mt-0 mb-3"><i class="icofont-phone me-2"></i></h1>
        <h2 class="mt-0 mb-2">Call {{ $contact[0]->info }} to get free access</h2>
        <p class="small mt-0 mb-4">{{ $banner[5]->deskripsi }}</p>
        <a href="#" class="btn btn-primary px-4">{{ $button[1]->nama }}</a>
    </div>
    <div class="bg-images-cover"></div>
</div>
<!-- Section > Pricing -->
<div id="pricingSection" class="d-block py-lg-5 py-3">
    <div class="container my-lg-5 my-4 px-lg-5">
        <h2 class="mb-lg-5 mb-4">Pricing</h2>
        <div class="row">
            @foreach ($price as $item)
            <div class="col-md-4 mb-lg-0 mb-4">
                <div class="card h-100 border-0">
                    <div class="card-body">
                        <div class="d-flex flex-column p-lg-3 h-100">
                            <div class="flex-1 mb-4">
                                <h5>{{ $item->nama }}</h5>
                                <div class="d-block text-lg font-thin">
                                    ${{ $item->harga }}/month
                                </div>
                                <ul class="list-group list-group-flush">
                                    @foreach ($item->privileges as $privilege)
                                    <li class="list-group-item border-bottom px-0">{{ $privilege -> privilege }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="d-block w-100 text-center">
                                <a href="#" class="btn btn-primary d-block mb-2">{{ $button[2]->nama }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<div class="d-block bg-light py-lg-5 py-3">
    <div class="container my-lg-5 my-4 px-lg-5">
        <div class="row justify-content-center">
            <div class="col-md-7">
                <div class="d-block text-center">
                    <h1 class="mb-3">{{ $banner[6]->judul }}</h1>
                    <p class="font-regular">{{ $banner[6]->deskripsi }}</p>
                    <a href="#" class="btn btn-primary btn-lg px-4">{{ $button[3]->nama }} <i class="icofont-arrow-right ms-3"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="d-block section-xlg bg-images" style="background-image: url('{{ $image[2]->gambar }}');"></div>
<div class="d-block bg-primary text-white py-lg-5 py-3">
    <div id="contactSection" class="container px-lg-5">
        <div class="d-flex d-flex-mobile align-items-start justify-content-center">
            <div class="mx-3 text-center mb-lg-0 mb-3">
                <small class="font-weight-bold"><i class="icofont-phone me-2"></i>{{ $contact[0]->nama }}</small>
                <div class="d-block font-weight-bold">{{ $contact[0]->info }}</div>
            </div>
            <div class="mx-3 text-center mb-lg-0 mb-3">
                <small class="font-weight-bold"><i class="icofont-envelope me-2"></i>{{ $contact[1]->nama }}</small>
                <div class="d-block font-weight-bold">{{ $contact[1]->info }}</div>
            </div>
            <div class="mx-3 text-center mb-lg-0 mb-3">
                <small class="font-weight-bold"><i class="icofont-location-pin me-2"></i>{{ $contact[2]->nama }}</small>
                <div class="d-block font-weight-bold">{{ $contact[2]->info }}</div>
            </div>
        </div>
    </div>
</div>
<!-- Navigation -->
<div class="nav-top bg-primary px-lg-4 px-2 pb-5 pt-5">
    <div class="nav-inner h-100">
        <div class="container-fluid h-100">
            <div class="row h-100">
                <div class="col-4 h-100">
                    <div class="d-flex align-items-center h-100">
                        <a href="#" class="nav-logo text-white">
                            Crativa
                        </a>
                    </div>
                </div>
                <div class="col-4 h-100">
                    <div class="d-flex align-items-center justify-content-center h-100">
                        <div class="nav-top-menus nav-top-menus--secondary">
                            <a href="#featuresSection" class="menus-link me-3">Features</a>
                            <a href="#pricingSection" class="menus-link me-3">Pricing</a>
                            <a href="#contactSection" class="menus-link">Contact</a>
                        </div>
                    </div>
                </div>
                <div class="col-4 h-100">
                    <div class="d-flex align-items-center justify-content-end h-100">
                        <div class="nav-top-menus nav-top-menus--white">
                            <a href="#" class="menus-link me-3"><i class="icofont-instagram"></i></a>
                            <a href="#" class="menus-link"><i class="icofont-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection