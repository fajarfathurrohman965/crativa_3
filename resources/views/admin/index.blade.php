@extends('layout.admin')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title">
          <span class="page-title-icon bg-gradient-primary text-white me-2">
            <i class="mdi mdi-home"></i>
          </span> Dashboard
        </h3>
        <nav aria-label="breadcrumb">
          <ul class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">
              <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
            </li>
          </ul>
        </nav>
      </div>
      <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
          <div class="card" style="height: 500px; overflow-y: auto;">
            <div class="card-body">
              <h4 class="card-title">Preview Dasboard</h4>
              <div class="table-responsive">
                <iframe src="{{ route('dashboard') }}" frameborder="0" style="width: 100%; height: 400px;"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection