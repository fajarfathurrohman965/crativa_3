@extends('layout.admin')

@section('content')
<div class="main-panel">
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white me-2">
            <i class="mdi mdi-file-tree"></i>
        </span> Contact
        </h3>
        <nav aria-label="breadcrumb">
          <ul class="breadcrumb">
          </ul>
        </nav>
      </div>
      <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <form class="form-inline" action="{{ $contact ? route('contact.update', $contact->id) : route('contact.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" name="nama" id="" class="form-control" placeholder="" value="{{ $contact ? $contact->nama : old('nama') }}" required aria-describedby="helpId">
                    </div>
                    <br>
                
                    <div class="form-group">
                        <label for="">Info</label>
                        <input type="text" name="info" id="" class="form-control" placeholder="" value="{{ $contact ? $contact->info : old('info') }}" required aria-describedby="helpId">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-gradient-primary btn-sm">Submit</button>
                    <button type="reset" class="btn btn-gradient-dark btn-sm">Reset</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection