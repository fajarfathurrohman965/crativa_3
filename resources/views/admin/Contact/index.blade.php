@extends('layout.admin')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title">
          <span class="page-title-icon bg-gradient-primary text-white me-2">
            <i class="mdi mdi-contacts"></i>
          </span> Contact
        </h3>
        <nav aria-label="breadcrumb">
          <ul class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">
              <a type="button" class="btn btn-gradient-info btn-icon-text btn-sm" href="{{ route('contact.create') }}">
                <i class="mdi mdi-folder-plus btn-icon-prepend"></i>Tambah</a>
            </li>
            </li>
          </ul>
        </nav>
      </div>
      <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th> No </th>
                      <th> Nama </th>
                      <th> Info </th>
                      <th> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($contact as $index=>$item)
                    <tr>
                        <td>{{ $index+1 }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>{{ $item->info }}</td>
                        <td>
                            <a type="button" class="btn btn-gradient-primary btn-icon-text btn-sm" href="{{ route('contact.edit', $item->id) }}">
                            <i class="mdi mdi-lead-pencil btn-icon-prepend"></i>Edit</a>
                            <a type="button" class="btn btn-gradient-danger btn-icon-text btn-sm" href="javascript:void(0);" onclick="deleteData({{ $item->id }})">
                              <i class="mdi mdi-delete-forever btn-icon-prepend"></i>Hapus
                            </a>
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
      function deleteData(id) {
          var confirmation = window.confirm("Apakah Anda yakin?\nData akan dihapus secara permanen!");
  
          if (confirmation) {
              // Redirect ke route delete dengan menyertakan parameter id
              window.location.href = "{{ url('admin/contact/delete') }}/" + id;
              // Jika menggunakan AJAX, Anda dapat menambahkan logika untuk menangani respons atau menampilkan alert sukses setelah penghapusan
          }
      }
  </script>
@endsection