@extends('layout.auth')
@section('content')
<form class="pt-3" action="authenticate" method="POST">
    @csrf
    <div class="form-group">
        <input type="email" class="form-control form-control-lg" id="youremail" name="email" placeholder="Email" required>
        <div class="invalid-feedback">Please enter your email.</div>
    </div>
    <div class="form-group">
        <input type="password" class="form-control form-control-lg" id="yourPassword" name="password" placeholder="Password" required>
        <div class="invalid-feedback">Please enter your password.</div>
    </div>
    <div class="mt-3">
        <button class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" type="submit">SIGN IN</button>
    </div>
    <div class="my-2 d-flex justify-content-between align-items-center">
        <div class="form-check">
            <label class="form-check-label text-muted" for="rememberMe">
                <input type="checkbox" class="form-check-input" name="remember" value="true" id="rememberMe"> Remember me </label>
        </div>
    </div>
</form>
@endsection
