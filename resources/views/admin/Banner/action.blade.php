@extends('layout.admin')

@section('content')
<div class="main-panel">
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white me-2">
            <i class="mdi mdi-file-tree"></i>
        </span> Banner
        </h3>
        <nav aria-label="breadcrumb">
          <ul class="breadcrumb">
          </ul>
        </nav>
      </div>
      <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <form class="form-inline" action="{{ $banner ? route('banner.update', $banner->id) : route('banner.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="">Judul</label>
                        <input type="text" name="judul" id="" class="form-control" placeholder="" value="{{ $banner ? $banner->judul : old('judul') }}" required aria-describedby="helpId">
                    </div>
                    <br>
                
                    <div class="form-group">
                        <label for="">Deskripsi</label>
                        <textarea name="deskripsi" id="" class="form-control" placeholder="" {{ $banner ? '' : 'nullable' }} aria-describedby="helpId">{{ $banner ? $banner->deskripsi : old('deskripsi') }}</textarea>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-gradient-primary btn-sm">Submit</button>
                    <button type="reset" class="btn btn-gradient-dark btn-sm">Reset</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection