@extends('layout.admin')

@section('content')
<div class="main-panel">
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white me-2">
            <i class="mdi mdi-file-tree"></i>
        </span> Button
        </h3>
        <nav aria-label="breadcrumb">
          <ul class="breadcrumb">
          </ul>
        </nav>
      </div>
      <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <form class="form-inline" action="{{ $button ? route('button.update', $button->id) : route('button.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="">Button</label>
                        <input type="text" name="nama" id="" class="form-control" placeholder="" value="{{ $button ? $button->nama : old('nama') }}" required aria-describedby="helpId">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-gradient-primary btn-sm">Submit</button>
                    <button type="reset" class="btn btn-gradient-dark btn-sm">Reset</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection