@extends('layout.admin')

@section('content')
<div class="main-panel">
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white me-2">
            <i class="mdi mdi-cash-multiple"></i>
            </span> Price
        </h3>
        <nav aria-label="breadcrumb">
          <ul class="breadcrumb">
          </ul>
        </nav>
      </div>
      <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <form class="form-inline" action="{{ $price ? route('price.update', $price->id) : route('price.store')}}" method="post">
                    @csrf
                    @if ($price)
                        <input type="text" name="id" value="{{ $price -> id }}" hidden>
                    @endif
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" name="nama" id="" class="form-control" placeholder="" value="{{ $price ? $price->nama : old('nama') }}" required aria-describedby="helpId">
                    </div>
                    <br>
                
                    <div class="form-group">
                        <label for="">Harga</label>
                        <input type="number" name="harga" id="" class="form-control" placeholder="" value="{{ $price ? $price->harga : old('harga') }}" required aria-describedby="helpId">
                    </div>
                    <div class="col-md-12 mb-3 mt-3" id="parentSubdesc">
                        @if ($price)
                            @foreach ($price->privileges as $item)
                                <div class="d-flex justify-content-end my-2 Subdesc">
                                    <div class="form-floating col-md-10">
                                        <input type="text" class="form-control" name="privilage[]" value="{{ $item ? $item->privilege : old('privilege') }}" placeholder="Privilage">
                                        <label>Privilege</label>
                                    </div>
                                    <button type="button" class="btn btn-gradient-success mx-2 my-2 btn-sm" id="plus"><i class="mdi mdi-plus-circle-outline"></i></button>
                                    <button type="button" class="btn btn-gradient-danger mx-2 my-2 btn-sm" id="minus"><i class="mdi mdi-minus-circle-outline"></i></button>
                                </div>
                            @endforeach
                        @else
                            <div class="d-flex justify-content-end my-2 Subdesc">
                                <div class="form-floating col-md-10">
                                    <input type="text" class="form-control" name="privilage[]" value="{{ $price ? $price->title : ''}}" placeholder="Privilage">
                                    <label>Privilege</label>
                                </div>
                                <button type="button" class="btn btn-gradient-success mx-2 my-2 btn-sm" id="plus"><i class="mdi mdi-plus-circle-outline"></i></button>
                                <button type="button" class="btn btn-gradient-danger mx-2 my-2 btn-sm" id="minus"><i class="mdi mdi-minus-circle-outline"></i></button>
                            </div>
                        @endif
                    </div>                    
                    <br>
                    <button type="submit" class="btn btn-gradient-primary btn-sm">Submit</button>
                    <button type="reset" class="btn btn-gradient-dark btn-sm">Reset</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    jQuery(document).ready(function($) {
        // Tambahkan kolom
        $("#plus").click(function() {
            var newColumn = $(".Subdesc:first").clone();
            newColumn.find("input").val(""); // Reset nilai input
            $("#parentSubdesc").append(newColumn);
        });

        // Hapus kolom
        $("#parentSubdesc").on("click", ".minus", function() {
            if ($("#parentSubdesc .Subdesc").length > 1) {
                $(this).closest(".Subdesc").remove();
            }
        });
    });
</script>
@endsection