@extends('layout.admin')

@section('content')
<div class="main-panel">
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white me-2">
            <i class="mdi mdi-file-tree"></i>
        </span> Feature
        </h3>
        <nav aria-label="breadcrumb">
          <ul class="breadcrumb">
          </ul>
        </nav>
      </div>
      <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <form class="form-inline" action="{{ $feature ? route('feature.update', $feature->id) : route('feature.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="">Judul</label>
                        <input type="text" name="judul" id="" class="form-control" placeholder="" value="{{ $feature ? $feature->judul : old('judul') }}" required aria-describedby="helpId">
                    </div>
                    <br>
                
                    <div class="form-group">
                        <label for="">Sub</label>
                        <input type="text" name="sub" id="" class="form-control" placeholder="" value="{{ $feature ? $feature->sub : old('sub') }}" required aria-describedby="helpId">
                    </div>
                    <br>
                
                    <div class="form-group">
                        <label for="">Deskripsi</label>
                        <textarea name="deskripsi" id="" class="form-control" placeholder="" required aria-describedby="helpId">{{ $feature ? $feature->deskripsi : old('deskripsi') }}</textarea>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-gradient-primary btn-sm">Submit</button>
                    <button type="reset" class="btn btn-gradient-dark btn-sm">Reset</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection