<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use App\Models\Image;
use App\Models\Price;
use App\Models\Banner;
use App\Models\Button;
use App\Models\Contact;
use App\Models\Feature;
use App\Models\Privilages;
use App\Models\Privileges;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // user
        User::create(
            [
                'name' => 'Mike Wazowski',
                'email' => 'admin@crativa.com',
                'password' => Hash::make('admin123')
            ]
        );

        // feature
        Feature::create(
            [
                'judul' => 'one',
                'sub' => 'Break the rules on the go anytime flawless',
                'deskripsi' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum laudantium sitatibus voluptates optioulla.'
            ]
        );
        Feature::create(
            [
                'judul' => 'two',
                'sub' => 'Install the connection everywhere',
                'deskripsi' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum laudantium sitatibus voluptates optioulla.'
            ]
        );
        Feature::create(
            [
                'judul' => 'three',
                'sub' => 'Launch play mode and tell your friends',
                'deskripsi' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum laudantium sitatibus voluptates optioulla.'
            ]
        );
        Feature::create(
            [
                'judul' => 'Why use Crativa?',
                'sub' => 'Good for technology enthusiast',
                'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat a fuga corrupti inventore officia porro nulla.'
            ]
        );
        Feature::create(
            [
                'judul' => 'How is Crativa?',
                'sub' => 'Easy to development in the future',
                'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat a fuga corrupti inventore officia porro nulla.'
            ]
        );
        Feature::create(
            [
                'judul' => 'Where to use Crativa?',
                'sub' => 'When find people around you',
                'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat a fuga corrupti inventore officia porro nulla.'
            ]
        );
        Feature::create(
            [
                'judul' => 'When to use Crativa?',
                'sub' => 'Anytime when connection avalaible',
                'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat a fuga corrupti inventore officia porro nulla.'
            ]
        );

        // image
        Image::create(
            [
                'gambar' => 'http://crativa.com/upload/1704443607banner-1.jpeg'
            ]
        );
        Image::create(
            [
                'gambar' => 'http://crativa.com/upload/1704443615banner-2.jpeg'
            ]
        );
        Image::create(
            [
                'gambar' => 'http://crativa.com/upload/1704443619banner-3.jpeg'
            ]
        );
        Image::create(
            [
                'gambar' => 'http://crativa.com/upload/1704443623banner-4.jpeg'
            ]
        );
        Image::create(
            [
                'gambar' => 'http://crativa.com/upload/1704443627banner-5.jpeg'
            ]
        );

        // price
        Price::create(
            [
                'nama' => 'Intro',
                'harga' => 5
            ]
        );
        Price::create(
            [
                'nama' => 'Medium',
                'harga' => 15
            ]
        );
        Price::create(
            [
                'nama' => 'Well Done',
                'harga' => 40
            ]
        );

        // contact
        Contact::create(
            [
                'nama' => 'Phone',
                'info' => '+62 22 1234 56789'
            ]
        );
        Contact::create(
            [
                'nama' => 'Email',
                'info' => 'crativakitchen@mail.com'
            ]
        );
        Contact::create(
            [
                'nama' => 'Location',
                'info' => 'Jakarta, Indonesia'
            ]
        );

        // banner
        Banner::create(
            [
                'judul' => 'Technology for the people future',
                'deskripsi' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eligendi blanditiis sapiente non excepturi.'
            ]
        );
        Banner::create(
            [
                'judul' => 'We specialize take over people power to enhance futurism mind.'
            ]
        );
        Banner::create(
            [
                'judul' => 'Meals that you have to serve before work, study and meeting with friends together. All in your hand.'
            ]
        );
        Banner::create(
            [
                'judul' => 'Launch play mode and tell your friends',
                'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat a fuga corrupti inventore officia porro nulla omnis molestiae doloremque nihil officiis ex, nulla laborum voluptatibus ab maxime reprehenderit accusantium. Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
            ]
        );
        Banner::create(
            [
                'judul' => 'Launch play mode and tell your friends',
                'deskripsi' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat a fuga corrupti inventore officia porro nulla omnis molestiae doloremque nihil officiis ex, nulla laborum voluptatibus ab maxime reprehenderit accusantium. Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
            ]
        );
        Banner::create(
            [
                'judul' => 'Call  to get free access',
                'deskripsi' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
            ]
        );
        Banner::create(
            [
                'judul' => 'Get the future people power free access now',
                'deskripsi' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam iure perferendis molestias tempore.'
            ]
        );

        // button
        Button::create(
            [
                'nama' => 'Sign Up'
            ]
        );
        Button::create(
            [
                'nama' => 'Contact Us'
            ]
        );
        Button::create(
            [
                'nama' => 'Choose This Plan'
            ]
        );
        Button::create(
            [
                'nama' => 'Signup Now'
            ]
        );
        Button::create(
            [
                'nama' => 'Learn More'
            ]
        );

        // privilege
        Privileges::create(
            [
                'price_id' => 1,
                'privilege' => 'Up to 4 connection'
            ]
        );
        Privileges::create(
            [
                'price_id' => 2,
                'privilege' => 'Up to 6 connection'
            ]
        );
        Privileges::create(
            [
                'price_id' => 2,
                'privilege' => 'Free installation'
            ]
        );
        Privileges::create(
            [
                'price_id' => 2,
                'privilege' => 'Special meals every 1 week'
            ]
        );
        Privileges::create(
            [
                'price_id' => 3,
                'privilege' => 'Up to 12 connection'
            ]
        );
        Privileges::create(
            [
                'price_id' => 3,
                'privilege' => 'Free installation'
            ]
        );
        Privileges::create(
            [
                'price_id' => 3,
                'privilege' => 'Special people every 1 week'
            ]
        );
        Privileges::create(
            [
                'price_id' => 3,
                'privilege' => 'Free futurism'
            ]
        );
    }
}
