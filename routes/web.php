<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\PriceController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\ButtonController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\FeatureController;
use App\Http\Controllers\AuthenticationController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [UserController::class, 'Dashboard'])->name('dashboard');

Route::get('login', [AuthenticationController::class, 'form'])->name('login')->middleware('guest');
Route::post('authenticate', [AuthenticationController::class, 'authenticate']);
Route::get('logout', [AuthenticationController::class, 'logout'])->middleware('auth')->name('logout');


Route::middleware('auth')->group(function () {
    Route::get('/admin', [AdminController::class, 'Index'])->name('admin');
    // admin-feature
    Route::group(['prefix' => 'admin', 'as' => 'feature.'], function(){
        Route::get('/feature', [FeatureController::class, 'index'])->name('index');
        Route::get('/feature/create', [FeatureController::class, 'create'])->name('create');
        Route::post('/feature/store', [FeatureController::class, 'store'])->name('store');
        Route::get('/feature/edit/{feature}', [FeatureController::class, 'edit'])->name('edit');
        Route::post('/feature/update/{feature}', [FeatureController::class, 'update'])->name('update');
        Route::get('/feature/delete/{feature}', [FeatureController::class, 'destroy'])->name('delete');
    });    
    // admin-image
    Route::group(['prefix' => 'admin', 'as' => 'image.'], function(){
        Route::get('/image', [ImageController::class, 'index'])->name('index');
        Route::get('/image/create', [ImageController::class, 'create'])->name('create');
        Route::post('/image/store', [ImageController::class, 'store'])->name('store');
        Route::get('/image/edit/{image}', [ImageController::class, 'edit'])->name('edit');
        Route::post('/image/update/{image}', [ImageController::class, 'update'])->name('update');
        Route::get('/image/delete/{image}', [ImageController::class, 'destroy'])->name('delete');
    }); 
    // admin-price
    Route::group(['prefix' => 'admin', 'as' => 'price.'], function(){
        Route::get('/price', [PriceController::class, 'index'])->name('index');
        Route::get('/price/create', [PriceController::class, 'create'])->name('create');
        Route::post('/price/store', [PriceController::class, 'store'])->name('store');
        Route::get('/price/edit/{price}', [PriceController::class, 'edit'])->name('edit');
        Route::post('/price/update/{price}', [PriceController::class, 'update'])->name('update');
        Route::get('/price/delete/{price}', [PriceController::class, 'destroy'])->name('delete');
    });
    // admin-contact
    Route::group(['prefix' => 'admin', 'as' => 'contact.'], function(){
        Route::get('/contact', [ContactController::class, 'index'])->name('index');
        Route::get('/contact/create', [ContactController::class, 'create'])->name('create');
        Route::post('/contact/store', [ContactController::class, 'store'])->name('store');
        Route::get('/contact/edit/{contact}', [ContactController::class, 'edit'])->name('edit');
        Route::post('/contact/update/{contact}', [ContactController::class, 'update'])->name('update');
        Route::get('/contact/delete/{contact}', [ContactController::class, 'destroy'])->name('delete');
    });
    // admin-banner
    Route::group(['prefix' => 'admin', 'as' => 'banner.'], function(){
        Route::get('/banner', [BannerController::class, 'index'])->name('index');
        Route::get('/banner/create', [BannerController::class, 'create'])->name('create');
        Route::post('/banner/store', [BannerController::class, 'store'])->name('store');
        Route::get('/banner/edit/{banner}', [BannerController::class, 'edit'])->name('edit');
        Route::post('/banner/update/{banner}', [BannerController::class, 'update'])->name('update');
        Route::get('/banner/delete/{banner}', [BannerController::class, 'destroy'])->name('delete');
    });
    // admin-button
    Route::group(['prefix' => 'admin', 'as' => 'button.'], function(){
        Route::get('/button', [ButtonController::class, 'index'])->name('index');
        Route::get('/button/create', [ButtonController::class, 'create'])->name('create');
        Route::post('/button/store', [ButtonController::class, 'store'])->name('store');
        Route::get('/button/edit/{button}', [ButtonController::class, 'edit'])->name('edit');
        Route::post('/button/update/{button}', [ButtonController::class, 'update'])->name('update');
        Route::get('/button/delete/{button}', [ButtonController::class, 'destroy'])->name('delete');
    });
});